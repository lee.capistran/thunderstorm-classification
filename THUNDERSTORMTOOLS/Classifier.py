#!/home/guillermo.valdes/.conda/envs/hht-py37/bin/python
#!/usr/bin/env python
# coding: utf-8
# Usage:
# ./Features channel gps duration 
# Example 1:
# ./Features L1:PEM-EY_MIC_VEA_PLUSY_DQ 1129945218 11

import pandas as pd
import numpy as np
import pickle
import argparse
import librosa
import csv
import os

from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

from gwpy.timeseries import TimeSeries

# command line options
parser = argparse.ArgumentParser()
parser.add_argument("channel", help="channel name")
parser.add_argument("gps", help="centered gps time", type=int)
parser.add_argument("duration", help="duration of segment", type=float)
args = parser.parse_args()
print(args)

# define parameters
chname = args.channel
gps  = args.gps
dur = args.duration

# set minimum duration
gpss = gps - 1
gpse = gps + 10

# load timeseries for witness channel
mic = TimeSeries.get(chname, gpss, gpse)
data = mic.bandpass(2, 1000)
data = data.astype('float32',casting='same_kind')
# print(mic.dtype)
# print(data.dtype)

# audio signal parameters
fs = data.sample_rate.value
xa = data.value

# extrancting features in segments
coeff = librosa.feature.mfcc(y=xa, sr=fs, n_mfcc=10, hop_length=32768*2)
chroma = librosa.feature.chroma_cens(y=xa, sr=fs, hop_length=32768*2, n_chroma=8)
contrast = librosa.feature.spectral_contrast(y=xa, sr=fs, hop_length=32768*2, n_bands=6)
centroid = librosa.feature.spectral_centroid(y=xa, sr=fs, hop_length=32768*2)
flatness = librosa.feature.spectral_flatness(y=xa, hop_length=32768*2)

rms = np.array([np.sqrt(np.mean(xa**2))])
maximum = np.array([np.max(np.abs(xa))])

# concatenate
feat1 = np.concatenate(coeff)
feat2 = np.concatenate(chroma)
feat3 = np.concatenate(contrast)
feat4 = np.concatenate(centroid)
feat5 = np.concatenate(flatness)

# setting features
features = np.concatenate([feat1,feat2,feat3,feat4,feat5,rms, maximum])
features = np.round(features, decimals=3)
features = features.reshape(1, -1)

# classifying 
classes = {0: 'other', 1: 'thunder'}

# loading model
loaded_model = pickle.load(open('knnpickle_file', 'rb'))
result = loaded_model.predict(features) 
print(result)
