#!/home/guillermo.valdes/.conda/envs/hht-py37/bin/python
# 
# Copyright (C) LIGO Scientific Collaboration (2019-)
#
# This code extract the audio features of a segment of 
# centered at the time given in <gps>
# duration equals to <duration>
#
# The features are stored in a file of name <gps>.csv
#
# Guillermo Valdes / Brina Martinez / Lee Ann Capistran / Apr 2021
# guillermo.valdes@ligo.org / brina.martinez@ligo.org / lee.capistran@ligo.org
# 

from gwtrigfind import find_trigger_files
from gwpy.table import EventTable
from gwpy.time import tconvert, to_gps
import argparse
import numpy as np
import pandas as pd
import sys
import csv
import os

from gwpy.segments import DataQualityDict
from gwpy.timeseries import TimeSeries
import matplotlib.pyplot as plt
from gwpy.table import Table
from gwpy.plot import Plot
from gwpy.time import tconvert, to_gps
import IPython
from gwpy.segments import DataQualityFlag
from gwpy.timeseries import TimeSeries                          
from gwpy.plot import Plot

parser = argparse.ArgumentParser()
parser.add_argument("gps", help="beginning gps time", type=int)
parser.add_argument("gpse", help="ending gps time", type=int)
#parser.add_argument("duration", help="duration of segment", type=float)
args = parser.parse_args()
#print(args)

# define parameters
gps_b = args.gps
gps_e = args.gpse
#dur = args.duration

#gps_e = gps + dur/2
#gps_b = gps - dur/2

l1segs = DataQualityFlag.query('L1:DMT-ANALYSIS_READY:1', gps_b,gps_e)
print(abs(l1segs.active))
splot = l1segs.plot()
splot.show()

mic_data = 'L1:PEM-EY_MIC_VEA_PLUSY_DQ'
cache = find_trigger_files(mic_data, 'Omicron', gps_b, gps_e)

table = EventTable.read(cache[0], nproc=16, tablename='sngl_burst', columns=['ifo','peak_time', 'peak_time_ns','start_time', 'snr', 'peak_frequency', 'param_one_value'])

aa = table['peak_time'] > gps_b
ab = table['peak_time'] < gps_e

bb = table['snr'] > 5.53
cc = aa & bb & ab

q = table[cc]

aa1 = table['peak_time'] > gps_b
ab1 = table['peak_time'] < gps_e
ac1 = [g not in l1segs.active for g in table['peak_time']]
bb1 = table['snr'] > 5.53
cc1 = aa1 & bb1 & ab1 & ac1

new = table[cc1]

def Remove(duplicate): 
    final_list = [] 
    for num in duplicate:
        if num not in final_list:
            final_list.append(num) 
    return final_list 
duplicate = np.array(new['peak_time'])
p = Remove(duplicate)
print(p)


# create file or append to file
filename = '%s.csv' % gps_b

if os.path.exists(filename):
    append_write = 'a' # append if already exists
else:
    append_write = 'w' # make a new file if not
reducedlist=[]
header = ['Triggered GPS Times']
# save file
with open('/home/lee.capistran/ClassifierwithOmicron/DraftCSV/%s.csv' % gps_b, mode=append_write) as omicrontriggers_file:
    omicrontriggers = csv.writer(omicrontriggers_file, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    omicrontriggers.writerow(i for i in header)
    omicrontriggers.writerows(map(lambda x: [x],p))
    